import abc

# 1
class Person(metaclass=abc.ABCMeta):
  @abc.abstractmethod
  def get_fullname():
    pass
  @abc.abstractmethod
  def add_request():
    pass
  @abc.abstractmethod
  def check_request():
    pass
  @abc.abstractmethod
  def add_user():
    pass

# 2
class Employee(Person):
  def __init__(self, firstname, lastname, email, department):
    self._firstname = firstname
    self._lastname = lastname
    self._email = email
    self._department = department

  def set_firstname(self, firstname):
    self._firstname = firstname
  def set_lastname(self, lastname):
    self._lastname = lastname
  def set_email(self, email):
    self._email = email
  def set_department(self, department):
    self._department = department

  def get_firstname(self):
    return self._firstname
  def get_lastname(self):
    return self._lastname
  def get_email(self):
    return self._email
  def get_department(self):
    return self._department

  # abstract methods implementations
  def get_fullname(self): 
    return f'{self._firstname} {self._lastname}'
  def add_request(self):
    return 'Request has been added'
  def check_request():
    pass
  def add_user():
    pass

  # additional methods
  def login(self):
    return f'{self._email} has logged in'
  def logout(self):
    return f'{self._email} has logged out'
  
  
# 3
class TeamLead(Person):
  def __init__(self, firstname, lastname, email, department):
    self._firstname = firstname
    self._lastname = lastname
    self._email = email
    self._department = department
    self._members_list = []

  def set_firstname(self, firstname):
    self._firstname = firstname
  def set_lastname(self, lastname):
    self._lastname = lastname
  def set_email(self, email):
    self._email = email
  def set_department(self, department):
    self._department = department

  def get_firstname(self):
    return self._firstname
  def get_lastname(self):
    return self._lastname
  def get_email(self):
    return self._email
  def get_department(self):
    return self._department
  def get_members(self):
    return self._members_list
  
  # abstract methods implementations
  def get_fullname(self):
    return f'{self._firstname} {self._lastname}'
  def add_request(self):
    return 'Request has been added'
  def check_request():
    pass
  def add_user():
    pass

  # additional methods
  def login(self):
    return f'{self._email} has logged in'
  def logout(self):
    return f'{self._email} has logged out'
  def add_member(self, employee):
    self._members_list.append(employee)

# 4
class Admin(Person):
  def __init__(self, firstname, lastname, email, department):
    self._firstname = firstname
    self._lastname = lastname
    self._email = email
    self._department = department

  def set_firstname(self, firstname):
    self._firstname = firstname
  def set_lastname(self, lastname):
    self._lastname = lastname
  def set_email(self, email):
    self._email = email
  def set_department(self, department):
    self._department = department

  def get_firstname(self):
    return self._firstname
  def get_lastname(self):
    return self._lastname
  def get_email(self):
    return self._email
  def get_department(self):
    return self._department
  
  # abstract methods implementations
  def get_fullname(self):
    return f'{self._firstname} {self._lastname}'
  def add_request():
    pass
  def check_request():
    pass
  def add_user(self):
    return 'User has been added'

  # additional methods
  def login(self):
    return f'{self._email} has logged in'
  def logout(self):
    return f'{self._email} has logged out'

# 5
class Request:
  def __init__(self, request, requester, date_requested, status):
    self._request = request
    self._requester = requester
    self._date_requested = date_requested
    self._status = status

  def set_request(self, request):
    self._request = request
  def set_requester(self, requester):
    self._requester = requester
  def set_date_requested(self, date_requested):
    self._date_requested = date_requested
  def set_status(self, status):
    self._status = status

  def get_request(self):
    return self._request
  def get_requester(self):
    return self._requester
  def get_date_requested(self):
    return self._date_requested
  def get_status(self):
    return self._status
  
  # additional methods
  def update_request(self):
    pass
  def close_request(self):
    return f'{self._request} Request Closed'
  def cancel_request(self):
    pass

# Test cases
employee1 = Employee('John', 'Doe', 'djohn@mail.com', 'Marketing')
employee2 = Employee('Jane', 'Smith', 'sjane@mail.com', 'Marketing')
employee3 = Employee('Robert', 'Patterson', 'probert@mail.com', 'Sales')
employee4 = Employee('Brandon', 'Smith', 'sbrandon@mail.com', 'Sales')
admin1 = Admin('Monika', 'Justin', 'jmonika@mail.com', 'Marketing')
team_lead1 = TeamLead('Michael', 'Specter', 'smichael@mail.com', 'Sales')
req1 = Request('New hire orientation', team_lead1, '27-Jul-2021', 'Pending')
req2 = Request('Laptop repair', employee1, '1-Jul-2021', 'Pending')

assert employee1.get_fullname() == 'John Doe', 'Full name should be John Doe'
assert admin1.get_fullname() == 'Monika Justin', 'Full name should be Monika Justin'
assert team_lead1.get_fullname() == 'Michael Specter', 'Full name should be Michael Specter'
assert employee2.login() == 'sjane@mail.com has logged in', 'check login' 
assert employee2.add_request() == 'Request has been added', 'check add request'
assert employee2.logout() == 'sjane@mail.com has logged out', 'check logout' 

team_lead1.add_member(employee3)
team_lead1.add_member(employee4)
for indiv_emp in team_lead1.get_members():
  print(indiv_emp.get_fullname())

assert admin1.add_user() == 'User has been added'

req2.set_status('closed')
print(req2.close_request())